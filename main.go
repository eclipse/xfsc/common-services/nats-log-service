package main

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.eclipse.org/eclipse/xfsc/common-services/eventlogservice/config"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"log"
	"strings"
)

func init() {
	config.LoadConfig()
}

func receive(event cloudevents.Event) {
	eventData := string(event.Data())

	for _, keyword := range config.CurrentLogServiceConfig.Keywords {
		if strings.Contains(strings.ToLower(eventData), strings.ToLower(keyword)) {
			log.Printf("Event with keyword %s detected: %s", keyword, event)
		}
	}
}

func main() {
	client, err := cloudeventprovider.NewClient(cloudeventprovider.Sub, "events")
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	log.Fatal(client.Sub(receive))
}
